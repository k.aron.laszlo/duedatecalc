const START = 9;
const END = 17;

/*=== this function I check the worker is in the working hours ===*/
const isWorkingHours = date => date.getUTCHours() >= START
    && (date.getUTCHours() < END
        || (
            date.getUTCHours() === END
            && date.getUTCMinutes() === 0
            && date.getUTCSeconds() === 0
            && date.getUTCMilliseconds() === 0)
    );

/*=== here I check it is weekday or not ===*/
const isWeekday = date => (date.getUTCDay() > 0 && date.getUTCDay() < 6);

const validatingSubmitDate = submitDate => (
    submitDate instanceof Date
    && typeof submitDate.getMonth === 'function'
    && isWeekday(submitDate)
    && isWorkingHours(submitDate)
);

/*=== check what day will be tommorow ===*/
const addDay = date => {
    date.setDate(date.getDate() + 1);
}

/*=== here we check its weekday or not, if it is, than i will subtrack the number of days ===*/
const addWorkDays = (date, days) => {
    while (days) {
        addDay(date);
        if (isWeekday(date)) {
            --days;
        }
    }
}

/*=== set the next work of day ===*/
const setToNextWorkDay = date => {
    addWorkDays(date, 1);
    date.setUTCHours(START);
    date.setUTCMinutes(0);
    date.setUTCSeconds(0);
    date.setUTCMilliseconds(0);
}

/*=== this function check how many working time does it have ===*/
const getRemaingWorkTime = (date) => {
    let day = new Date(date);
    day.setUTCHours(END);
    day.setUTCMinutes(0);
    day.setUTCSeconds(0);
    day.setUTCMilliseconds(0);
    return day.getTime() - date.getTime();
}

/*==== init function ====*/

const dueDateCalc = (submitDate, turnaround) => {
    if (isNaN(turnaround) || turnaround < 0)
        throw new Error("InvalidTurnaroundTime")

    if (!validatingSubmitDate(submitDate))
        throw new Error("InvalidSubmitDate");

    let result = new Date(submitDate);

    let turnaroundMillisLeft = turnaround * 60 * 60 * 1000;
    while (turnaroundMillisLeft > 0) {
        let remainingWorkTime = getRemaingWorkTime(result);
        let workTime = Math.min(remainingWorkTime, turnaroundMillisLeft);

        result.setTime(result.getTime() + workTime);

        turnaroundMillisLeft -= workTime;
        if (turnaroundMillisLeft > 0) {
            setToNextWorkDay(result);
        }
    }

    return result;
};

/*===== Simply check if its work well ======*/

const submitDate = new Date(Date.UTC(2018, 5, 8, 12, 30));
const turnaround = 8;
try {
    const dueDate = dueDateCalc(submitDate, turnaround);
    console.log("Due date: " + dueDate.toLocaleString());
} catch (e) {
    console.log("Some error happened: " + e);
}